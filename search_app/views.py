from django.http import HttpResponse
from django.shortcuts import render_to_response
from forms import *
import MAPI
from MAPI.Util import *
from MAPI.Tags import *

class Email():
	def __init__ (self,l):
		self.subject = l[0]
		self.sender = l[1]
		self.to = l[2]
		self.body = l[3]
		self.entryId = l[4]	
	
def hash(s):
	sum=0
	for x in s:
		sum+=ord(x)
		sum*=2
	return sum	
		
def doSearch(query,whichFolder):
	

	session = OpenECSession('saeed', 'saeidsaeid', 'http://localhost:236/zarafa')
	store = GetDefaultStore(session)
	

	folderId = 0
	
	if whichFolder=='inbox':	
		result = store.GetReceiveFolder('IPM', 0)
		folderId = result[0]
	elif whichFolder=='outbox':
		props = store.GetProps([PR_IPM_SENTMAIL_ENTRYID], 0)
		folderId = props[0].Value
		
		
	inbox = store.OpenEntry(folderId, None, 0)
	table = inbox.GetContentsTable(0)
	table.SetColumns([PR_ENTRYID, PR_SUBJECT,PR_SENDER_EMAIL_ADDRESS,PR_DISPLAY_TO,PR_BODY], 0)
	table.SortTable(SSortOrderSet( [ SSort(PR_CREATION_TIME, TABLE_SORT_DESCEND) ], 0, 0), 0);
	rows = table.QueryRows(10, 0)
	myResult = []
	for row in rows:
		if row[2].Value.find(query)>=0:
			myResult.append(Email((row[1].Value,row[2].Value,row[3].Value,row[4].Value,hash(row[0].Value))))
	return myResult
				 
		
def showEmail(emailId):
	session = OpenECSession('saeed', 'saeidsaeid', 'http://localhost:236/zarafa')
	store = GetDefaultStore(session)
	result = store.GetReceiveFolder('IPM', 0)
	folders = [ store.GetReceiveFolder('IPM', 0)[0], #inbox
				store.GetProps([PR_IPM_SENTMAIL_ENTRYID], 0)[0].Value ]
	for folderId in folders:
		folder=store.OpenEntry(folderId,None,0)
		table=folder.GetContentsTable(0)
		table.SetColumns([PR_ENTRYID, PR_SUBJECT,PR_SENDER_EMAIL_ADDRESS,PR_DISPLAY_TO,PR_BODY], 0)
		table.SortTable(SSortOrderSet( [ SSort(PR_CREATION_TIME, TABLE_SORT_DESCEND) ], 0, 0), 0);
		rows = table.QueryRows(10, 0)
		myResult = []
		for row in rows:
			e = Email((row[1].Value,row[2].Value,row[3].Value,row[4].Value,hash(row[0].Value)))
			if int(e.entryId) == int(emailId):
				return e


def index(request):
	form = SearchForm()
	showingResult = False
	if request.GET.has_key('id'):
		e = showEmail(request.GET['id'])
		return render_to_response('search_app/emailview.html',{'e':e})
		
	elif request.GET.has_key('query'):
		showingResult = True
		query = request.GET['query'].strip()
		inboxResults = doSearch(query,'inbox')
		outboxResults = doSearch(query,'outbox')
		form = SearchForm({'query':query})
		return render_to_response('search_app/index.html',{'form':form,'showingResult':showingResult,'inboxResults':inboxResults,'outboxResults':outboxResults})
	else:
		return render_to_response('search_app/index.html',{'form':form,'showingResult':showingResult})    
